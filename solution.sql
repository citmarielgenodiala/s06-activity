-- Mariel Genodiala BSIT- 3

-- I just chose to create a database for this one so I can double check my answers.
-- CREATE DATABASE solution;
-- USE solution;

-- CREATE TABLE author(
--     au_id INT NOT NULL,
--     au_lname VARCHAR(50) NOT NULL,
--     au_fname VARCHAR(50) NOT NULL,
--     address VARCHAR(100) NOT NULL,
--     city VARCHAR(100) NOT NULL,
--     state VARCHAR(50) NOT NULL,
--     PRIMARY KEY (au_id)
-- );

-- CREATE TABLE publisher(
--     pub_id INT NOT NULL,
--     pub_name VARCHAR(50) NOT NULL,
--     city VARCHAR(50) NOT NULL,
--     PRIMARY KEY (pub_id)
-- );

-- CREATE TABLE title(
--     title_id INT NOT NULL,
--     title VARCHAR(100) NOT NULL,
--     type VARCHAR(50) NOT NULL,
--     price DOUBLE NOT NULL,
--     pub_id INT NOT NULL,
--     PRIMARY KEY (title_id),
--     FOREIGN KEY (pub_id) REFERENCES publisher(pub_id)
-- );

-- CREATE TABLE author_title(
--     au_id INT NOT NULL,
--     title_id INT NOT NULL,
--     FOREIGN KEY (au_id) REFERENCES author(au_id),
--     FOREIGN KEY (title_id) REFERENCES title(title_id)
-- );

-- # 2. I did not include the IDs

    a. SELECT title.title FROM author JOIN author_title ON author.au_id = author_title.au_id JOIN title ON author_title.title_id = title.title_id 
        WHERE author.au_fname = 'Marjorie' AND author.au_lname = 'Green';
        Answers/Output: "The Busy Executive's Database Guide" and "You Can Combat Computer Stress!"

    b. SELECT title.title FROM author JOIN author_title ON author.au_id = author_title.au_id JOIN title ON author_title.title_id = title.title_id 
        WHERE author.au_fname = 'Michael' AND author.au_lname = "O'Leary";
        Answers/Output: "Cooking with Computers"

    c.SELECT author.au_fname, author.au_lname FROM author JOIN author_title ON author.au_id = author_title.au_id JOIN title ON author_title.title_id = title.title_id 
        WHERE title.title = "The Busy Executive's Database Guide";
        Answers/Output: "Marjorie Green" and "Abraham Bennet"

    d. SELECT publisher.pub_name FROM publisher JOIN title ON publisher.pub_id = title.pub_id WHERE title.title = 'But Is It User Friendly?';
        Answers/Output: "Algodata Infosystems"

    e. SELECT title.title FROM title JOIN publisher ON title.pub_id = publisher.pub_id WHERE publisher.pub_name = 'Algodata Infosystems';
        Answers/Output: "The Busy Executive's Database Guide", "Cooking with Computers", "Straight Talk About Computers", "But Is It User Friendly?", 
                         "Secrets of Silicon Valley", "Net Etiquete"

-- #3 
mysql - u root

CREATE DATABASE blog_db;
USE blog_db;

CREATE TABLE users(
    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(300) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id)
);
DESCRIBE users;

CREATE TABLE posts(
    id INT NOT NULL AUTO_INCREMENT,
    author_id INT NOT NULL,
    title VARCHAR(500) NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_post_author_id
    FOREIGN KEY (author_id) REFERENCES users(id) ON DELETE RESTRICT On UPDATE CASCADE
);
DESCRIBE posts;

CREATE TABLE post_comments(
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_commented DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_post_comments_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id) ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT fk_post_comments_user_id
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE RESTRICT ON UPDATE CASCADE
);
DESCRIBE post_comments;

CREATE TABLE post_likes(
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    datetime_liked DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_post_likes_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id) ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT fk_post_likes_user_id
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE RESTRICT ON UPDATE CASCADE
);
DESCRIBE post_likes;
SHOW tables;